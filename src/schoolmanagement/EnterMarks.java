package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class EnterMarks extends JFrame {
    private JTextField registrationNumberTxt;
    private JComboBox subjectList;
    private JTextField marksTxt;
    private JButton addMarksButton;
    private JButton cancelButton;
    private JPanel panel7;
    private JFrame frame7;

    public EnterMarks() {
        frame7 = new JFrame("Enter Marks");

        frame7.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame7.setPreferredSize(new Dimension(250,200));
        frame7.setResizable(false);


        frame7.add(panel7);

        frame7.pack();
        frame7.setLocationRelativeTo(null);
        frame7.setVisible(true);
    }

    public static void main(String[] args) {
        new EnterMarks();
    }
}
