package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class LoginPage extends JFrame{
    private JTextField userName;
    private JPasswordField password;

    private JButton loginButton;

    private JPanel panel3;
    private JPanel panel11;
    private JPanel panel22;
    private JFrame frame;

    public LoginPage() {

        frame = new JFrame("Login Page");

        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(250,200));
        frame.setResizable(false);


        frame.add(panel3);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);




    }

    public static void main(String[] args) {
        new LoginPage();
    }
}
