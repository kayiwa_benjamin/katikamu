package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class TeacherHomeScreen extends JFrame {
    private JButton enterMarksButton;
    private JButton viewStudentsButton;
    private JButton registerStudentButton;
    private JButton changePasswordButton;
    private JButton editStudentButton;
    private JButton logOutButton;
    private JPanel panel2;

    private JFrame frame3;

    public TeacherHomeScreen() {

        frame3 = new JFrame("Teacher's Home Screen");

        frame3.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame3.setPreferredSize(new Dimension(350,300));
        frame3.setResizable(false);


        frame3.add(panel2);

        frame3.pack();
        frame3.setLocationRelativeTo(null);
        frame3.setVisible(true);

    }

    public static void main(String[] args) {
        new TeacherHomeScreen();
    }
}
