package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class ChangePassword extends JFrame {
    private JPasswordField newpasswordField1;
    private JPasswordField newpasswordField2;
    private JButton changePasswordButton;
    private JButton cancelButton;
    private JPanel panel6;
    private JFrame frame6;

    public ChangePassword() {

        frame6 = new JFrame("Change Password");

        frame6.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame6.setPreferredSize(new Dimension(250,200));
        frame6.setResizable(false);


        frame6.add(panel6);

        frame6.pack();
        frame6.setLocationRelativeTo(null);
        frame6.setVisible(true);
    }

    public static void main(String[] args) {
        new ChangePassword();
    }
}
