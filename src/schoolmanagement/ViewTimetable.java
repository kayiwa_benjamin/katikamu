package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class ViewTimetable extends JFrame{
    private JTable timetable;
    private JPanel panel10;

    private JFrame frame10;

    public ViewTimetable() {

        frame10 = new JFrame("Timetable");

        frame10.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame10.setPreferredSize(new Dimension(350,300));
        frame10.setResizable(true);


        frame10.add(panel10);

        frame10.pack();
        frame10.setLocationRelativeTo(null);
        frame10.setVisible(true);
    }

    public static void main(String[] args) {
        new ViewTimetable();
    }
}
