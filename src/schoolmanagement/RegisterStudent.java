package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class RegisterStudent extends JFrame{
    private JTextField firstNameTxt;
    private JTextField lastNameTxt;
    private JTextField registrationNumberTxt;
    private JTextField ageTxt;
    private JTextField subjectTxt;
    private JButton registerButton;
    private JPanel panel1;
    private JPasswordField regpasswordField1;
    private JComboBox gendercomboBox1;
    private JFrame frame2;

    public RegisterStudent() {

        frame2 = new JFrame("Register Student");

        frame2.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame2.setPreferredSize(new Dimension(350,350));
        frame2.setResizable(false);


        frame2.add(panel1);

        frame2.pack();
        frame2.setLocationRelativeTo(null);
        frame2.setVisible(true);
    }

    public static void main(String[] args) {
        new RegisterStudent();
    }
}
