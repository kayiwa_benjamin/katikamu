package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class ViewMarks extends JFrame{
    private JList myMarks;
    private JPanel panel8;

    private JFrame frame8;

    public ViewMarks() {
        frame8 = new JFrame("View Marks");

        frame8.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame8.setPreferredSize(new Dimension(450,300));
        frame8.setResizable(true);


        frame8.add(panel8);

        frame8.pack();
        frame8.setLocationRelativeTo(null);
        frame8.setVisible(true);
    }

    public static void main(String[] args) {
        new ViewMarks();
    }
}
