package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class StudentHomeScreen extends JFrame{
    private JButton viewMarksButton;
    private JButton changePasswordButton;
    private JButton viewTimetableButton;
    private JButton logOutButton;
    private JPanel panel5;
    private JFrame frame5;

    public StudentHomeScreen() {

        frame5 = new JFrame("Student Home Screen");

        frame5.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame5.setPreferredSize(new Dimension(300,250));
        frame5.setResizable(false);


        frame5.add(panel5);

        frame5.pack();
        frame5.setLocationRelativeTo(null);
        frame5.setVisible(true);

    }

    public static void main(String[] args) {
        new StudentHomeScreen();
    }
}
