package schoolmanagement;

import javax.swing.*;
import java.awt.*;

public class ViewStudents extends JFrame {
    private JList studentlist;
    private JPanel panel9;

    private JFrame frame9;

    public ViewStudents() {

        frame9 = new JFrame("View Students");

        frame9.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame9.setPreferredSize(new Dimension(450,400));
        frame9.setResizable(true);


        frame9.add(panel9);

        frame9.pack();
        frame9.setLocationRelativeTo(null);
        frame9.setVisible(true);
    }

    public static void main(String[] args) {
        new ViewStudents();
    }
}
